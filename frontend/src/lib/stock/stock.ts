import type { Bean } from '.';
import { default as beans } from './beans_in.json' assert { type: 'json' };

async function get_stock() {
	let bean_arr: Array<Bean> = new Array();
	bean_arr = [beans][0];

	return bean_arr;
}

export { get_stock, Bean };
