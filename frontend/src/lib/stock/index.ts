import { get_stock } from './stock';

class Bean {
	name: string = '';
	amount_in_stock: number = 0;
}

export { get_stock, Bean };
